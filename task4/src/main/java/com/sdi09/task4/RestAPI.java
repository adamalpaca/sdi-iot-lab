package com.sdi09.task4;

import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@RestController
public class RestAPI {

    private ScoreRepo scoreRepository;

    public RestAPI(ScoreRepo scoreRepository) {
        this.scoreRepository = scoreRepository;
    }

    @GetMapping("/scorebook/")
    //public Collection<ScoreRecord> getAllScoreRecords()
    public Collection<ScoreRecord> getTop10()
    {
        return scoreRepository.findTop10ByOrderByScoreDesc();
    }

    @GetMapping("/scorebook/{name}")
    public Collection<ScoreRecord>  getScoreRecordByName(@PathVariable String name) {
        return scoreRepository.findAllByPlayerName(name); //.orElseThrow(RecordNotFoundException::new);
    }

    @PostMapping("/scorebook/")
    public void addScoreBookRecord(@RequestBody ScoreRecord record) {
        scoreRepository.insert(record);
    }

}