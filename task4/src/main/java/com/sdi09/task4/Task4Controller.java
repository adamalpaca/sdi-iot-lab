package com.sdi09.task4;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class Task4Controller {
    private int count = 0;

    @GetMapping("/")
    public String index(@RequestParam(required = false, defaultValue = "stranger") String name, Model model) {
        model.addAttribute("name", name);
        model.addAttribute("count", ++count);
        return "index";
    }

    /*
    @GetMapping("/error")
    public String error(@RequestParam(required = false, defaultValue = "stranger") String name, Model model) {
        model.addAttribute("name", name);
        model.addAttribute("count", ++count);
        return "index";
    }
    */
}
