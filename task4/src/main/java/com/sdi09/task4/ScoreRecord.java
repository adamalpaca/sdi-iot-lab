package com.sdi09.task4;

//import com.fasterxml.jackson.annotation.JsonProperty;
//import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "ScoreBook")
public class ScoreRecord {
    //@Id commented to have a random id
    private String id;
    private String playerName;
    private String score;

    public String getPlayerName() {
        return playerName;
    }

    public void setPlayerName(String playerName) {
        this.playerName = playerName;
    }

    public String getScore() {
        return score;
    }

    public void setScore(String score) {
        this.score = score;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }



}
