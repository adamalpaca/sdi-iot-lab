package com.sdi09.task4;

import org.springframework.boot.autoconfigure.data.web.SpringDataWebProperties;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;


@Repository
public interface ScoreRepo extends MongoRepository<ScoreRecord,String> {
    Collection<ScoreRecord> findTop10ByOrderByScoreDesc();
    Collection<ScoreRecord> findAllByPlayerName(String PlayerName);
}