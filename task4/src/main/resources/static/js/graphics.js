var config = {
    type: Phaser.AUTO,
    width: 800,
    height: 600,
    physics: {
        default: 'arcade',
        arcade: {
            gravity: { y: 0 }
        }
    },
    scene: {
        preload: preload,
        create: create
    }
};

var game = new Phaser.Game(config);
var gamePage = 0;
var physicsPage;

// Intro page - 0
var logo;
var emitter;
var particles;


// Game page - 1
//var _back;
var _head;
var _body;
var bodyParts;
var oldBodyParts;
var _food;
var _tail;
var tailDirection;

function preload ()
{
//    this.load.setBaseURL('http://labs.phaser.io');
//    this.load.image('sky', 'assets/skies/space3.png');
    this.load.image('sky', 'http://labs.phaser.io/assets/skies/nebula.jpg');
//    this.load.image('logo', 'assets/sprites/phaser3-logo.png');
    this.load.image('logo', 'images/snake.png');
    this.load.image('smoke', 'http://labs.phaser.io/assets/particles/smoke0.png');


    // TODO images for snake
//    this.load.image('_back', '');
    this.load.image('_head', 'images/head.png');
    this.load.image('_body', 'images/body.png');
    this.load.image('_food', 'images/food.png');
    this.load.image('_tail', 'images/wall.png');
}

function create ()
{
    // Make sure body is an array
    _body = [];

    // Background
    this.add.image(400, 300, 'sky');
    physicsPage = this.physics;

    // Intro screen
    particles = this.add.particles('smoke');
    emitter = particles.createEmitter({
        speed: 100,
        scale: { start: 1, end: 0 },
        blendMode: 'ADD'
    });
    logo = this.physics.add.image(400, 100, 'logo');
    logo.setVelocity(100, 200);
    logo.setBounce(1, 1);
    logo.setCollideWorldBounds(true);
    emitter.startFollow(logo);

    // Game screen
    _head = this.physics.add.image(400,100,'_head');
    _head.setAlpha(0);

    // Create food
    _food = this.physics.add.image(400,100,'_food');
    _food.setAlpha(0);

    // Create tail
    _tail = this.physics.add.image(400,100,'_tail');
    _tail.setAlpha(0);

    // Makes sure that the program doesn't try to show body parts that haven't loaded
    bodyParts = snakeLength-1;
    oldBodyParts = snakeLength-1;

    // Create the initial body parts
    for(var i=0; i<bodyParts; i++)
    {
        var elm = this.physics.add.image(400,100,'_body');
        if(i == 0)
        {
            _body[0] = elm;
        }
        else{
            _body.push(elm);
        }
        elm.setAlpha(0);
    }
}


function render()
{
    if(gamePage == 0){
        logo.destroy();
        particles.destroy();
        gamePage = 1;
        _head.setAlpha(100);
        _food.setAlpha(75);
        _tail.setAlpha(100);
        tailDirection = direction;
    }

    if(gamePage == 1) {
        for (var i = 0; i < BOARDHEIGHT; i++) {
            for (var j = 0; j < BOARDWIDTH; j++) {

                // Check if snake is head
                if(snake[i][j] == 1) {
                    _head.setPosition(SNAKEPIXELS/2+SNAKEPIXELS*j,SNAKEPIXELS/2+SNAKEPIXELS*i);
                    _head.setAngle(90*direction);
                }
                else if(snake[i][j] == -1) {
                    _food.setPosition(SNAKEPIXELS/2+SNAKEPIXELS*j,SNAKEPIXELS/2+SNAKEPIXELS*i);
                }
                else if(snake[i][j] > 1 && snake[i][j]<bodyParts+1){
                    _body[snake[i][j]-2].setAlpha(100);
                    _body[snake[i][j]-2].setPosition(SNAKEPIXELS/2+SNAKEPIXELS*j,SNAKEPIXELS/2+SNAKEPIXELS*i);
                }
                else if(snake[i][j] == bodyParts+1){
                    var xTemp = SNAKEPIXELS/2 + SNAKEPIXELS*j;
                    var yTemp = SNAKEPIXELS/2+ SNAKEPIXELS*i;
                    var xOld = _body[snake[i][j]-3].x;
                    var yOld = _body[snake[i][j]-3].y;

                    // Calculate tail orientation
                    if(yOld == yTemp)
                    {
                        if(xTemp>xOld){
                            tailDirection = 2;
                        }
                        else{
                            tailDirection = 0;
                        }
                    }
                    else if(yTemp > yOld) {
                        tailDirection = 1;
                    }
                    else{
                        tailDirection = 3;
                    }

                    _tail.setPosition(xTemp,yTemp);

                    // Check last body part to get tail angle right
                    _tail.setAngle(90*tailDirection);
                }
            }
        }
    }
}

function addBodyPart()
{
    var newGuy = physicsPage.add.image(20, 60, '_body');
    newGuy.setAlpha(0);
    _body.push(newGuy);
    bodyParts++;
}

function restartGraphics()
{
    // Delete the extra body parts
    for(var i=3; i<bodyParts; i++)
    {
        // TODO delete here instead of making it invisible
        _body[i].setAlpha(0);
    }

    bodyParts = snakeLength-1;
    oldBodyParts = snakeLength-1;
}