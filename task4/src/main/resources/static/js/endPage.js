function drawDiv(x, y, l, h, c, t, i)
{
    document.write(
        "<textarea id=" + i + " style='left:" +
        x + "px;top:" + y + "px;width:" + l +
        "px;height:" + h + "px;background:" +
        c + ";position:absolute;padding:10px'>" + t + "</textarea>"
    );
}


function showAllEndPage()
{
    $('#restartButton').show();
    $('#nameParagraph').show();
    $('#nameText').show();
    $('#scoreButton').show();

    $('#PlayerScoreParagraph').show();
    $('#findScoreText').show();
    $('#playerScoreButton').show();

    $('#allScoreParagraph').show();
    $('#allScoreButton').show();
}

function hideAllEndPage()
{
    $('#restartButton').hide();
    $('#nameParagraph').hide();
    $('#nameText').hide();
    $('#scoreButton').hide();
}