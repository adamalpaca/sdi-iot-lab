// Create a table for snake
var snake = [[]];       // -2 wall, -1 food, 0 space, 1 head , 2,3,4,5,... body
//var sideLength = 30;
var direction = STARTDIR;      // 0 down, 1 right, 2 up, 3 left
var snakeLength = SNAKESTARTLENGTH;
var snakeX = BOARDWIDTH/2;
var snakeY = BOARDHEIGHT/2;
var refreshInterval;
var notSpeed = 0;
var score = 0;

function startGame()
{
    hideAllStartPage();
//    $('#container').show();
    snakeX = BOARDHEIGHT/2;
    snakeY = BOARDWIDTH/2;
    snakeLength = 5;
    direction = STARTDIR;
    score = 0;
    notSpeed = 150
    setTable();

    $('#scoreSpan').text(score);

    $('#container').show();
    restartGraphics();
    render();
    refreshInterval = setInterval(moveForward, notSpeed);
}

function initialise(){
    for(var x=0; x<BOARDHEIGHT; x++) {
        if(x>0){
            snake.push([]);
        }

        for(var y=0; y<BOARDWIDTH; y++) {
            snake[x].push(0);
        }
    }
}

function setTable()
{
    // Set 0 everywhere
    for(var i=0; i<BOARDHEIGHT; i++){
        for(var j=0; j<BOARDWIDTH; j++){
            snake[i][j]=0;
        }
    }
    // Setup snake on board
    snake[snakeX][snakeY] = 1;
    for(var i=1; i<snakeLength; i++)
    {
        snake[snakeX-i][snakeY] = i+1;
    }
    placeFruit();
    //showAllEndPage();
}

function placeFruit() {
    // Place fruit
    var x = Math.random()*BOARDHEIGHT;
    var y = Math.random()*BOARDWIDTH;
    var xInt = parseInt(x, 10);
    var yInt = parseInt(y, 10);
    while (snake[xInt][yInt] > 0) {
        x = Math.random()*BOARDHEIGHT;
        y = Math.random()*BOARDWIDTH;
        xInt = parseInt(x, 10);
        yInt = parseInt(y, 10);
    }
    snake[xInt][yInt] = -1;
}

function turnLeft() {
    direction = (direction+1)%4;
}

function turnRight(){
    direction = direction===0? 3:(direction-1);
}

function moveForward() {

    // Move snake around
    for(var x=0; x<BOARDHEIGHT; x++){
        for(var y=0; y<BOARDWIDTH; y++) {
            // If there is a snake
            if(snake[x][y] > 0){
                snake[x][y] = snake[x][y] + 1;

                if(snake[x][y] > snakeLength)
                {
                    snake[x][y] = 0;
                }
            }
        }
    }

    // Update head
    switch(direction){
        case 0: // Right
            snakeX = (snakeX+1)%BOARDHEIGHT;
            break;
        case 1: // Up
            snakeY = snakeY===0?BOARDWIDTH-1:snakeY-1;
            break;
        case 2: // Left
            snakeX = snakeX===0?BOARDHEIGHT-1:snakeX-1;
            break;
        case 3:
            snakeY = (snakeY+1)%BOARDWIDTH;
            break;
        default:
    }

    // If you hit yourself, you die
    if(snake[snakeX][snakeY] > 0){
        clearInterval(refreshInterval);
        var timeout = setTimeout(endGame(),200);
    }
    // If there's fruit you can move forward & the score goes up
    else if(snake[snakeX][snakeY] < 0){
        snake[snakeX][snakeY] = 1;
        snakeLength++;
        score++;
        //increase the snake s speed as the score increases
        if (score % 5 == 0 && score <= 50)
        {
            clearInterval(refreshInterval);
            notSpeed = notSpeed - 15
            //if speed reach zero set it to 10
            if(notSpeed <= 0)
            {
                notSpeed = 10;
            }
            refreshInterval = setInterval(moveForward, notSpeed);
        }
        $('#scoreSpan').text(score);
        // Place another fruit
        placeFruit();
        addBodyPart();
    }

    // If it's empty, you can move forwards
    else{
        snake[snakeX][snakeY] = 1;
    }

    render();
}

/*
function render()
{
    var html = "<div class='snake-board'><table>";
    for (var i = 0; i < sideLength; i++) {
        html += "<tr id='row-" + i + "' class='row'>";
        for (var j = 0; j < sideLength; j++) {
            html += "<td id='" + snake[i][j] + (snake[i][j]>0? "' class='snake'></td>" : (snake[i][j]===0?"' class='col'></td>":"' class='fruit'></td>"));
        }
        html += "</tr>"
    }
    html += "</table></div>";
    document.getElementById('container').innerHTML = html;
}*/


function endGame(){
    showAllEndPage();
}

    //Enter a new score in the DB
    function submitScore()
    {
        //Text input in textArea
        var name = $('#nameText').val();
        $.ajax({
            url: "http://localhost:8080/scorebook/",
            type: "POST",
            contentType: "application/json; charset=UTF-8",
            data: JSON.stringify({'playerName' : name, 'score' : score}),
            success: function(data){
                //console.log(data);
            }
        });

        console.log( name + ' : '+ score);

    }

    //display top 10 scores
    function top10ScoreDisplay() {
        drawDiv(10, 220, 200, 100, "lightblue", "", "ScoreBoard");
        $.get("http://localhost:8080/scorebook/",
            function(data){
                $("#ScoreBoard").text(data.map(function(record){
                    return record.playerName + " : " + record.score;
                }).join('\n'));
            });
    }

    //Display all scores of a selected player
    function playerScoreDisplay() {
        $.get("http://localhost:8080/scorebook/" + $("#findScoreText").val(),
            function(data){
                $("#ScoreBoard").text(data.map(function(record){
                    return record.playerName + " : " + record.score;
                }).join('\n'));
            });

        drawDiv(10, 220, 200, 100, "lightblue", "", "ScoreBoard");

    }

