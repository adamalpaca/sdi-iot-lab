var client = new Paho.MQTT.Client("vlesdi.hevs.ch", 15675, "/ws", "trubsdflkoihisohieg");
var number = 0;
client.onConnectionLost = function(responseObject) {
    if (responseObject.errorCode !== 0) {
        console.error("Lost connection:" + responseObject.errorMessage);
        connectClient();
    } else {
        console.log("Connection closed.");
    }
};

client.onMessageArrived = function(message) {
    console.log("Got message: topic=" + message.destinationName + ', payload=' +
        message.payloadString);

    var splitMessage = message.destinationName.split('/');

    if(splitMessage[2].localeCompare("button") === 0)
    {
        number++;

        console.log("About to test the thingy addresses : "+splitMessage[1]);

        if(splitMessage[1].localeCompare("F4:1E:FB:87:27:F6") === 0)
        {
            console.log(splitMessage[1] + " pressed");
            console.log(message.payloadString);
            if(message.payloadString === "true") {
                turnLeft();
            }
        }
        else if(splitMessage[1].localeCompare("D3:63:90:36:DA:50") === 0){
            console.log(splitMessage[1] + " pressed");
            if(message.payloadString === "true") {
                turnRight();
            }
        }
    }

};

connectClient();

function connectClient()
{
    client.connect({
        userName: 'sdi09',
        password: '18681422d3fc90fcb3bed30757aebb3f',
        cleanSession: true,
        onSuccess: function () {
            console.log("Connected.");
            client.subscribe("sdi09/hello/status");
            client.subscribe("sdi09/+/button");
            client.send('sdi09/F4:1E:FB:87:27:F6/led', JSON.stringify({
                red: 255,
                green: 0,
                blue: 19
            }));
            client.send('sdi09/D3:63:90:36:DA:50/led', JSON.stringify({
                red: 50,
                green: 255,
                blue: 100
            }));
        },
        onFailure: function () {
            console.error("Failed to connect.");
        }
    });
}